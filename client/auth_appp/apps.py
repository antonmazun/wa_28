from django.apps import AppConfig


class AuthApppConfig(AppConfig):
    name = 'auth_appp'
