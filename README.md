1) Нужно затянуть проект себе локально на комп
    -- если нет гита https://git-scm.com/book/ru/v2/%D0%92%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5-%D0%A3%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0-Git
    
2) После успешной установки выполните в любой пустой папке с консоли команду -- git clone https://gitlab.com/antonmazun/wa_28.git

3) Сoздайте РЯДОМ с этим проектом виртуальное окружение и активируйте его
    -- python3 -m venv env (macOS  , Linux) 
        -- source env/vin/activate
        
    -- python -m venv env (Windows)
        -- cd env/Scripts затем команда activate
        
4) Затем вам нужно установить все зависимости , которые есть в проектe. Файл зависимостей называется req.txt . там , где находится этот файл с консоли нужно выполнить команду
-- pip install -r req.txt

5) После успешной установки всех зависимостей нужно сделать миграции.
    -- python manage.py migrate
    -- python manage.py makemigrations
    -- python manage.py migrate
    
6) Создать суперпользователя
    -- python manage.py createsuperuser
    
7) Запустить сервер
    -- python manage.py runserver
    
    
http://localhost:8000  - по этому адресу должно все заработать )